-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2021 at 04:03 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cus`
--

-- --------------------------------------------------------

--
-- Table structure for table `members_info`
--

CREATE TABLE `members_info` (
  `ID` int(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `cnum` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bday` date NOT NULL,
  `gndr` varchar(255) NOT NULL,
  `mnstry` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `members_info`
--

INSERT INTO `members_info` (`ID`, `fname`, `lname`, `cnum`, `address`, `bday`, `gndr`, `mnstry`) VALUES
(5, 'Junna', 'Kudo', '07044354563', 'Chiba-ken Yachiyo-shi Katsutadai 3-37-8 Happiness House Before 1-105', '1997-06-26', 'Male', 'Youth'),
(6, 'Junnel', 'Kudo', '07044354563', 'Chiba-ken Yachiyo-shi Katsutadai 3-37-8 Happiness House Before 1-105', '1995-09-10', 'Male', 'Youth'),
(29, 'Pamela Jane', 'Punzalan', '08062846953', 'Chiba-ken Yachiyo-shi Katsutadai 3-37-8 Happiness House Before 1-105', '2002-07-21', 'Female', 'Youth'),
(31, 'Maria Avelina', 'Punzalan', '07044354563', 'Chiba-ken Yachiyo-shi Katsutadai 3-37-8 Happiness House Before 1-105', '1965-08-06', 'Female', 'Women');

-- --------------------------------------------------------

--
-- Table structure for table `registered_users`
--

CREATE TABLE `registered_users` (
  `id` int(8) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_users`
--

INSERT INTO `registered_users` (`id`, `user_name`, `display_name`, `password`, `admin`) VALUES
(4, 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', '1'),
(5, 'usher', 'Usher', '0d5e410c96a560d494fe2b3485f5f864', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members_info`
--
ALTER TABLE `members_info`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `registered_users`
--
ALTER TABLE `registered_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members_info`
--
ALTER TABLE `members_info`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `registered_users`
--
ALTER TABLE `registered_users`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

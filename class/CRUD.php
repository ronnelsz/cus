<?php
namespace Phppot;

use \Phppot\DataSource;

class CRUD
{

  private $dbConn;

  private $ds;

  function __construct()
  {
    require_once "DataSource.php";
    $this->ds = new DataSource();
  }

  function addUp($query, $paramType="", $paramArray="")
  {
    $userResult = $this->ds->insert($query, $paramType, $paramArray);
    return true;
  }
  function show($query){
    $userResult = $this->ds->select($query);
    return $userResult;
  }
  function delete($query)
  {
    $userResult = $this->ds->execute($query);
    return true;
  }
}

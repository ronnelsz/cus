<?php
namespace Phppot;

use \Phppot\DataSource;

class User
{

  private $dbConn;

  private $ds;

  function __construct()
  {
    require_once "DataSource.php";
    $this->ds = new DataSource();
  }

  function getUserById($userId)
  {
    $query = "select * FROM registered_users WHERE id = ?";
    $paramType = "i";
    $paramArray = array($userId);
    $userResult = $this->ds->select($query, $paramType, $paramArray);

    return $userResult;
  }

  public function processLogin($username, $password) {
    $passwordHash = md5($password);
    $query = "select * FROM registered_users WHERE user_name = ? AND password = ?";
    $paramType = "ss";
    $paramArray = array($username, $passwordHash);
    $userResult = $this->ds->select($query, $paramType, $paramArray);
    if(!empty($userResult)) {
      $_SESSION["userId"] = $userResult[0]["id"];
      $_SESSION["userAdmin"] = $userResult[0]["admin"];
      $date = date("Y-m-d");
      $query = "UPDATE a_date SET save_date = '$date'";
      $isSuccess = $this->ds->execute($query);
      if(empty($_SESSION["date"]))$_SESSION["date"] = $date;
      return true;
    }
  }
}

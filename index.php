<?php
session_start();
if(!empty($_SESSION["userId"])) {
  if($_SESSION["userAdmin"] == 1)
    require_once './view/dashboardAdmin.php';
  else
    require_once './view/dashboardUsher.php';
} else {
    require_once './view/login-form.php';
}
?>

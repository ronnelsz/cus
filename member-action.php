<?php
namespace Phppot;

use Phppot\CRUD;
require_once (__DIR__ . "./class/CRUD.php");
$crud = new CRUD();

if(isset($_GET["delete"])){
  $query = "DELETE FROM `members_info` WHERE ID = " . $_POST["id"];
  $isSuccess = $crud->delete($query);
}
else{
  $mFname = $_POST["mFname"];
  $mLname = $_POST["mLname"];
  $mCNum = $_POST["mCNum"];
  $mAdd = $_POST["mAdd"];
  $mBday = $_POST["mBday"];
  $mGndr = $_POST["mGndr"];
  $mMinistry = $_POST["mMinistry"];
  $arrayInfo1 = array($mFname, $mLname, $mCNum, $mAdd, $mBday, $mGndr, $mMinistry);

  if(isset($_GET["update"])){
    $mId = $_POST["mId"];
    $query = "UPDATE `members_info` SET `fname`=?,`lname`=?,`cnum`=?,`address`=?,`bday`=?,`gndr`=?,`mnstry`=? WHERE ID = " . $mId;
    $paramType = "sssssss";
    $isSuccess = $crud->addUp($query, $paramType, $arrayInfo1);
  }
  else{
    $query = "INSERT INTO members_info (`fname`, `lname`, `cnum`, `address`, `bday`, `gndr`, `mnstry`) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $paramType = "sssssss";
    $isSuccess = $crud->addUp($query, $paramType, $arrayInfo1);
    if(! $isSuccess){

    }
    header("Location: ./index.php");
    exit();
  }
}

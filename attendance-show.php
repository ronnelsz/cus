<?php
namespace Phppot;

use Phppot\CRUD;
require_once (__DIR__ . "/class/CRUD.php");
$crud = new CRUD();
$date = $_SESSION["date"];
if($att_show == "1"){
  $query = "SELECT * FROM members_info mi WHERE NOT EXISTS (SELECT * FROM attendance a WHERE a.member_id = mi.ID AND a.date_attended = '$date') ORDER BY fname";
  $isSuccess = $crud->show($query);
  if(!empty($isSuccess)){
    for ($i=0; $i < count($isSuccess) ; $i++) {
      $name = $isSuccess[$i]["fname"] . " " . $isSuccess[$i]["lname"];
      $id = $isSuccess[$i]["ID"];
      echo "<tr> <td> <a class='aEdit' onClick='aModal(\"$name\", $id )'> Attend </a> </td> <td>" . $name . "</td>";
    }
  }
}
else{
  $query = "SELECT mi.fname, mi.lname, a.temp, a.ID FROM members_info mi INNER JOIN attendance a ON mi.ID = a.member_id AND a.date_attended = '$date'";
  $isSuccess = $crud->show($query);
  if(!empty($isSuccess)){
    for ($i=0; $i < count($isSuccess) ; $i++) {
      $name = $isSuccess[$i]["fname"] . " " . $isSuccess[$i]["lname"];
      $id = $isSuccess[$i]["ID"];
      $temp = $isSuccess[$i]["temp"];
      echo "<tr> <td> <a class='aEdit' onClick='aDel($id)'> Delete </a> </td> <td> $name </td> <td>$temp</td>";
    }
  }
}

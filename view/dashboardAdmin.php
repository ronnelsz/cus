<?php
namespace Phppot;

use \Phppot\User;

if (! empty($_SESSION["userId"])) {
  require_once __DIR__ . './../class/User.php';
  $user = new User();
  $userResult = $user->getUserById($_SESSION["userId"]);
  if(!empty($userResult[0]["display_name"])) {
    $displayName = ucwords($userResult[0]["display_name"]);
  } else {
    $displayName = $userResult[0]["user_name"];
  }
}
?>
<html>
<head>
  <title>User Login</title>
  <link href="./view/css/style.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
  <div>
    <div class="dashboard">
      <div class="member-dashboard">Welcome <b><?php echo $displayName; ?></b>, You have successfully logged in!<br>
        Click to <a href="logout.php" class="logout-button">Logout</a>
      </div>
    </div>
  </div>
  <div>
    <div class="dashboard">
      <!-- Tab links -->
      <div class="tab">
        <button class="tablinks" onclick="openAdminTab(event, 'MembersInfo')">Members Info</button>
        <button class="tablinks" onclick="openAdminTab(event, 'Reports')">Reports</button>
        <button class="tablinks" onclick="openAdminTab(event, 'Settings')">Settings</button>
      </div>
      <!-- Tab content -->
      <div id="MembersInfo" class="tabcontent">
        <div class="mInfoDiv">
          <form action="member-action.php" method="post" onSubmit="return validate();">
            <h3>Add Members Information</h3>
            <input type="hidden" id="mId">
            <input type="text" id="mFname" name="mFname" placeholder="Firstname" class="input-box">
            <input type="text" id="mLname" name="mLname" placeholder="Lastname" class="input-box"> <br>
            <input type="text" id="mCNum" name="mCNum" placeholder="Contact Number" class="input-box">
            <input type="text" id="mAdd" name="mAdd" placeholder="Address" id="txtBox" class="input-box">  <br>
            Birthdate: <input type="date" id="mBday" name="mBday" class="input-box bday" oninput='style.color="black"'> <br>
            <div class="wrapper" id="wrapper">
              Gender:
              <input type="radio" name="mGndr" value="Male" id="option-1">
              <input type="radio" name="mGndr" value="Female" id="option-2">
              <label for="option-1" class="option option-1">
                <div class="dot"></div>
                <span>Male</span>
              </label>
              <label for="option-2" class="option option-2">
                <div class="dot"></div>
                <span>Female</span>
              </label>
            </div> <br>
            Ministry:
            <select class="input-box bday" id="mMinistry" name="mMinistry" oninput='style.color="black"'>
              <option hidden> Select...  </option>
              <option value="Men"> Men </option>
              <option value="Women"> Women </option>
              <option value="Youth"> Youth </option>
              <option value="Music"> Music </option>
              <option value="Ushering"> Ushering </option>
              <option value="Presiding"> Presiding </option>
            </select> <br> <br>

            <input type="submit" class="btn" id='mSvUp' name="" value="Save">
            <input type="button" class="btn" name="" value="Cancel" onClick="mCancel()">
          </form>
        </div>
        <div class="mInfoDiv">
          Search: <input type="text" id="sInfo" class="" oninput='mSearch()'>
          <br>
          <table class="fl-table" id='tblInfo'>
            <thead>
              <tr>
                <th> Name </th>
                <th> Gender </th>
                <th> Birthday </th>
                <th> Ministry </th>
                <th> Button </th>
              </tr>
            </thead>
            <?php
            require 'member-show.php';
            ?>
          </table>
        </div>
      </div>

      <div id="Reports" class="tabcontent">
        <?php
        require 'view/report.php';
        ?>
      </div>

      <div id="Settings" class="tabcontent">
        <h3>Tokyo</h3>
        <p>Tokyo is the capital of Japan.</p>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  function openAdminTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    var display = tabName != "MembersInfo" ? "block" : "flex";
    document.getElementById(tabName).style.display = display;
    evt.currentTarget.className += " active";
  }
  var mId = document.getElementById("mId");
  var mFname = document.getElementById("mFname");
  var mLname = document.getElementById("mLname");
  var mCNum = document.getElementById("mCNum");
  var mAdd = document.getElementById("mAdd");
  var mBday = document.getElementById("mBday");
  var mMinistry = document.getElementById("mMinistry");
  var mSvUp = document.getElementById("mSvUp");
  var mM = document.getElementById("option-1");
  var mF = document.getElementById("option-2");
  function validate() {
    if(mSvUp.value == "Save"){
      var valid = true;
      var txtBox = document.getElementsByClassName("input-box");
      var rb = mM.checked == true ? true : mF.checked == true ? true : false;
      valid = rb;
      if(!rb)
      document.getElementById("wrapper").style.border = "red 1px solid";
      else
      document.getElementById("wrapper").style.border = "none";
      for(var i=0; i<txtBox.length; i++){
        var txtBox1 = txtBox[i].value;
        if(txtBox1 == "" || txtBox1 == "Select..."){
          txtBox[i].style.border = "red 1px solid";
          valid = false;
        }
        else{
          txtBox[i].style.border = "#CCC 1px solid";
        }
      }
    }
    else{
      valid = false;
      var rb = mM.checked == true ? "Male" : "Female";
      $.post('../cus/member-action.php?update=yes',{'mId':mId.value, 'mFname':mFname.value, 'mLname':mLname.value, 'mCNum':mCNum.value, 'mAdd':mAdd.value, 'mBday':mBday.value, 'mMinistry':mMinistry.value, 'mGndr':rb},function(data){
        alert("Updated.");
        window.location.reload(false);
      });
    }
    return valid;
  }
  function mUpdate(id){
    $.getJSON('../cus/member-show.php?mid='+ id + '',function(data){
      mId.value = data[0]["ID"];
      mFname.value = data[0]["fname"];
      mLname.value = data[0]["lname"];
      mCNum.value = data[0]["cnum"];
      mAdd.value = data[0]["address"];
      mBday.style = "color: black";
      mBday.value = data[0]["bday"];
      mMinistry.style = "color: black";
      mMinistry.value = data[0]["mnstry"];
      mSvUp.value = "Update";
      if(data[0]["gndr"] == "Male")
      mM.checked = true;
      else
      mF.checked = true;
    });
  }
  function mDelete(id){
    var cn = confirm("Are you sure you want to delete this?");
    if(cn){
      $.post('../cus/member-action.php?delete=yes',{'id': id},function(data){
        alert("Deleted.");
        window.location.reload(false);
      });
    }
  }
  function mSearch(){
    var srch = document.getElementById("sInfo");
    var val = srch.value.toLowerCase();
    var valArr = val.split(' ');
    var tbl = document.getElementById('tblInfo');
    var tblLength = tbl.rows.length;
    if (tblLength != 0) {
      for (var i = 1; i < tblLength; i++) {
        tbl.rows[i].style.display = 'table-row';
        for (var j = 0; j < valArr.length; j++) {
          if (tbl.rows[i].textContent.toLowerCase().indexOf(valArr[j]) === -1) {
            tbl.rows[i].style.display = 'none';
          }
        }
      }
    }
  }
  function mCancel(){
    mId.value = '';
    mFname.value = '';
    mLname.value = '';
    mCNum.value = '';
    mAdd.value = '';
    mBday.value = '';
    mBday.style = "color: gray";
    mMinistry.options[0].selected = true;
    mMinistry.style = "color: gray";
    mM.checked = false;
    mF.checked = false;
    mSvUp.value = "Save";
  }
</script>
</body>
</html>

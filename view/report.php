<div
id="genChart" style="width:100%; max-width:600px; height:500px; background-color:"#d2edd5"">
</div>

<div
id="minsChart" style="width:100%; max-width:600px; height:500px; background-color:"#d2edd5"">
</div>

<script>
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(genChart);
google.charts.setOnLoadCallback(minsChart);

function genChart() {
var data = google.visualization.arrayToDataTable([
  ['Gender', 'gndr'],
  ['Men',48.6],
  ['Women',54.8],
]);

var options = {
  title:'Gender',
  backgroundColor: '#d2edd5',
  height:500,
  width:500,
  is3D:true,
  legend: {
          position: 'left',
        alignment: 'center' ,
        orientation: 'horizontal',
    }
};

var chart = new google.visualization.PieChart(document.getElementById('genChart'));
  chart.draw(data, options);
}

function minsChart() {
var data = google.visualization.arrayToDataTable([
  ['Ministry', 'mnstry'],
  ['Youth',37.6],
  ['Children',20.8],
  ['Elders',54.8],
]);

var options = {
  title:'Ministry',
  backgroundColor: '#d2edd5',
  height:500,
  width:500,
  is3D:true,
  legend: {
          position: 'left',
        alignment: 'center' ,
        orientation: 'horizontal',
    }
};

var chart = new google.visualization.PieChart(document.getElementById('minsChart'));
  chart.draw(data, options);
}
</script>

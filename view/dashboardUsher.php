<?php
namespace Phppot;

use \Phppot\User;

if (! empty($_SESSION["userId"])) {
  require_once __DIR__ . './../class/User.php';
  $user = new User();
  $userResult = $user->getUserById($_SESSION["userId"]);
  if(!empty($userResult[0]["display_name"])) {
    $displayName = ucwords($userResult[0]["display_name"]);
  } else {
    $displayName = $userResult[0]["user_name"];
  }
}
?>
<html>
<head>
  <title>User Login</title>
  <link href="./view/css/style.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
  <div>
    <div class="dashboard divide3">
      <div class="member-dashboard dDivide3">
        <img src='../cus/img/logo.png' height="90">
      </div>
      <div class="member-dashboard dDivide3">Welcome <b><?php echo $displayName; ?></b>, You have successfully logged in!<br>
        Click to <a href="logout.php" class="logout-button">Logout</a>
      </div>
      <div class="member-dashboard dDivide3">
        <label> <?php echo date("l M d, Y") ?></label>
      </div>
    </div>
  </div>
  <div>
    <div class="dashboard">
      <!-- Tab links -->
      <div class="tab">
        <button class="tablinks" onclick="openUsherTab(event, 'Attendance')">Attendance</button>
        <button class="tablinks" onclick="openUsherTab(event, 'Reports')">Reports</button>
        <button class="tablinks" onclick="openUsherTab(event, 'Settings')">Settings</button>
      </div>
      <!-- Tab content -->
      <div id="Attendance" class="tabcontent">
        Date: <input type="date" id="aDate" value=<?php $date_edit = 1; require "attendance-action.php"; ?> onchange="aCDate()"> <br>
        <div id="attendanceDiv">
          <div class="mInfoDiv">
            Search: <input type="text" id='sAtt' oninput='aSearch("sAtt","tblAtt")'>
            <br>
            <table class="fl-table" id = "tblAtt">
              <thead>
                <tr>
                  <th> Action </th>
                  <th> Name </th>
                </tr>
              </thead>
              <?php $att_show = "1"; require "attendance-show.php"; ?>
            </table>
          </div>
          &nbsp
          <fieldset class="mInfoDiv">
            <legend> Attended </legend>
            Search: <input type="text" id="sAtt1" oninput='aSearch("sAtt1","tblAtt1")'>
            <br>
            <table class="fl-table" id="tblAtt1">
              <thead>
                <tr>
                  <th> Action </th>
                  <th> Name </th>
                  <th> Temp </th>
                </tr>
              </thead>
              <?php $att_show = "2"; require "attendance-show.php"; ?>
            </table>
          </div>
        </fieldset>
      </div>

      <div id="Reports" class="tabcontent">
        <h3>Paris</h3>
        <p>Paris is the capital of France.</p>
      </div>

      <div id="Settings" class="tabcontent">
        <h3>Tokyo</h3>
        <p>Tokyo is the capital of Japan.</p>
      </div>
    </div>
  </div>
  <!-- The Modal -->
  <div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header">
        <span class="close">&times;</span>
        <h2>Modal Header</h2>
      </div>
      <div class="modal-body">
        Attendance (<label id="aName"> </label>) <br>
        <input type="hidden" id="aID" value="">
        Temperature: <input type="text" id="aTemp"> <br>
        <input type="button" class="btn" value="Save" onClick="aSave()">
        <input type="button" class="btn" value="Cancel" onClick="aCancel()">
      </div>
      <div class="modal-footer">
        <h3>Modal Footer</h3>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  function openUsherTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
  }

  var aDate = document.getElementById("aDate");
  function aModal(name,id){
    document.getElementById("aName").innerHTML = name + "<br> Date: " + aDate.value;
    document.getElementById("aID").value = id;
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
      modal.style.display = "none";
    }
  }
  function aSave(){
    var aid = document.getElementById("aID").value;
    var adate = document.getElementById("aDate").value;
    var atemp = document.getElementById("aTemp").value;
    $.post("../cus/attendance-action.php?save=yes",{id:aid, date:adate, temp:atemp}, function(data){
      alert("Success");
      window.location.reload(false);
    });
  }
  function aSearch(idTxt, idTbl){
    var srch = document.getElementById(idTxt);
    var val = srch.value.toLowerCase();
    var valArr = val.split(' ');
    var tbl = document.getElementById(idTbl);
    var tblLength = tbl.rows.length;
    if (tblLength != 0) {
      for (var i = 1; i < tblLength; i++) {
        tbl.rows[i].style.display = 'table-row';
        for (var j = 0; j < valArr.length; j++) {
          if (tbl.rows[i].textContent.toLowerCase().indexOf(valArr[j]) === -1) {
            tbl.rows[i].style.display = 'none';
          }
        }
      }
    }
  }
  function aCDate(){
    var cdate1 = aDate.value;
    $.post("../cus/attendance-action.php?cdate="+cdate1,function(data){
      window.location.reload(false);
    });
  }
  function aDel(id){
    var cn = confirm("Are you sure you want to delete this?");
    if(cn){
      $.post("../cus/attendance-action.php?id="+id, function(data){
        alert("Success");
        window.location.reload(false);
      });
    }
  }
</script>
</body>
</html>
